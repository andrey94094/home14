let a = {
    b: 3,
    c: 4,
    d: 7,
}

let e = a.b;
for (key in a) {
    if (e > a[key]) { e = a[key] }
}
for (key in a) {
    if (e === a[key]) { delete a[key] }
}
//---2

const b = {
    ...a, f: { h: 'hi', j: 123 }
}
// ---3
b.f.j = e;
//---4
if (a.b === undefined) { console.log('not') }
else { console.log('yes') }
//---5
let aa = {b:5}
const ab = { b: 78 }
aa = { ...aa, ...ab }
